from flask import Flask, jsonify, request
import studentServices

app = Flask(__name__)


@app.route('/rest/find-all-students', methods=["GET"])
def findAllStudents():
    return jsonify(studentServices.findAllStudents())


@app.route("/rest/find-students-by-id/<studentId>", methods=["GET"])
def findStudentById(studentId):
    # print(studentId)
    return jsonify(studentServices.findStudentById(studentId))


@app.route("/rest/add-student", methods=["POST"])
def addStudent():
    student = request.get_json()
    if (type(student) == list):
        return jsonify(studentServices.addStudents(student))
    return jsonify(studentServices.addStudent(student))


@app.route("/rest/update-student", methods=["PUT"])
def updateStudent():
    student = request.get_json()
    s = (student["name"], student["age"], student["city"], student["id"])
    return jsonify(studentServices.updateStudent(s))


@app.route("/rest/delete-student/<studentId>", methods=["DELETE"])
def deleteStudent(studentId):
    return jsonify(studentServices.deleteStudentById(studentId))


if __name__ == "__main__":
    app.run("localhost", 8080, True)
