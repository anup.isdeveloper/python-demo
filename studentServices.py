import MySqlConfig


def createDataBase(dbName):
    cursor = MySqlConfig.getDB().cursor()
    cursor.execute("CREATE DATABASE " + dbName)
    cursor.close()


def createTable(tableName):
    cursor = MySqlConfig.getDB().cursor()
    cursor.execute("CREATE TABLE " + tableName + " (name VARCHAR(255), age INT, city VARCHAR(255))")
    cursor.close()


def showTableList():
    cursor = MySqlConfig.getDB().cursor()
    cursor.execute("SHOW TABLES")
    for x in cursor:
        print(x)
    cursor.close()


def addStudent(student):
    cursor = MySqlConfig.getDB().cursor()
    student = (student["name"], student["age"], student["city"])
    sql = "INSERT INTO  STUDENTS (name, age, city) VALUES (%s, %s, %s)"
    cursor.execute(sql, student)
    MySqlConfig.getDB().commit()
    # print(cursor.rowcount, "record inserted.")
    cursor.close()
    return cursor.rowcount


def addStudents(students):
    cursor = MySqlConfig.getDB().cursor()
    studentList = list()
    for s in students:
        student = (s["name"], s["age"], s["city"])
        studentList.append(student)
    sql = "INSERT INTO STUDENTS (name, age, city) VALUES (%s, %s, %s)"
    cursor.executemany(sql, studentList)
    MySqlConfig.getDB().commit()
    # print(cursor.rowcount, "record inserted.")
    cursor.close()
    return cursor.rowcount


def findAllStudents():
    cursor = MySqlConfig.getDB().cursor()
    cursor.execute("SELECT * FROM STUDENTS")
    result = cursor.fetchall()
    studentList = list()
    for s in result:
        student = {"name": s[0], "age": s[1], "city": s[2]}
        studentList.append(student)
    return studentList

def findStudentById(studentId):
    cursor = MySqlConfig.getDB().cursor()
    cursor.execute("SELECT * FROM STUDENTS WHERE id=" + str(studentId))
    result = cursor.fetchall()[0]
    return {"name": result[0], "age": result[1], "city": result[2]}


def deleteStudentById(studentId):
    cursor = MySqlConfig.getDB().cursor()
    sql = "DELETE FROM STUDENTS WHERE id = " + str(studentId)
    cursor.execute(sql)
    MySqlConfig.getDB().commit()
    return cursor.rowcount


def updateStudent(student):
    cursor = MySqlConfig.getDB().cursor()
    sql = "UPDATE STUDENTS SET name = %s, age = %s, city = %s WHERE id = %s"
    cursor.execute(sql, student)
    MySqlConfig.getDB().commit()
    #print(cursor.rowcount, "record(s) affected")
    return cursor.rowcount


def deleteTable(tableName):
    cursor = MySqlConfig.getDB().cursor()
    cursor.execute("DROP TABLE " + tableName)
